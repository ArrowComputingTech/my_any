#!/home/hz/.rbenv/shims/ruby

require './eech.rb'

module Enumerable
  def my_any?
    retr = []
    self.each do |x|
      if yield(x)
        puts "Succeeded: #{x}"
        retr.push(self[x])
      else
        puts "Failed: #{x}"
      end
    end
    return retr.length > 0 ? true : false
  end
end

arr = [5,4]
puts arr.my_any? { |x| x >= 3 }
puts arr.my_any? { |x| x <= 3 }
