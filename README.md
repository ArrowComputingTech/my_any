This Ruby program evaluates all elements in an array.  It returns true if any of the elements return true from the supplied block, false otherwise.
